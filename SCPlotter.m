clc;
close all;
clear all;

%% Constants
V1 = 4.2:-0.01:3.7;
V2 = 3.7;
C = 100e-6;
f = 1000;


%% Calculate
Ib = C*(V1-V2)/2*f;

%% Plot
figure;
plot(V1-V2, Ib);