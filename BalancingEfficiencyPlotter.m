clc;
close all;
clear all;

%% Constants
N = 2:2:12;

%% Generate data
x = -1:0.01:1;
qk = min(1, 1+x);

i=1;
for nn=N
    n(i, :) = (nn.*qk)./(x+nn);
    i = i+1;
end

%% Plot data
plot(x, n, 'LineWidth', 1.5);
xlabel("Single cell capacity imbalance (x)");
ylabel("\eta_{balancing}");
title("Passive balancing efficiency");
legend("N = 2", "N = 4", "N = 6", "N = 8", "N = 10", "N = 12")
